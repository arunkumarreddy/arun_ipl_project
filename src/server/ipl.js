const csv=require('csvtojson') ;
const fs = require('fs')
const deliveriesPath = "../data/deliveries.csv" ;
const matchesPath = "../data/matches.csv" ;
csv()
.fromFile(deliveriesPath)
.then((deliveriesdata)=>{
csv()
.fromFile(matchesPath)
.then((matchesdata)=>{
    //matchesPlayedPerYear(matchesdata);
    //matchesOwnByTeam(matchesdata);
    //extraRunsConceededByTeam(matchesdata,deliveriesdata);
    topTenEconomicalBowlers(matchesdata,deliveriesdata);

});
});


function matchesPlayedPerYear(matchesdata)
{
    let op={};
	for(let index=0;index<matchesdata.length;index++)
	{
		let countCheck=0;
		let atributeReq = matchesdata[index].season;
		for(const atribute in op)
		{
			if(atribute === atributeReq) {
				countCheck += 1;
			}
		}
		if(countCheck == 0)
		{
			op[atributeReq]=1
		}
		else
		{
			op[atributeReq] += 1;
		}
	}

fs.writeFile('../output/output3.json', JSON.stringify(op), 'utf-8', function(err) {
	if (err) throw err
	console.log('Done!')
})
}



function extraRunsConceededByTeam(matchesdata,deliveriesdata)
{
  let op = {} ;
  for(let index=0;index<matchesdata.length;index++) 
  {
    if(matchesdata[index].season === '2016' )
    {
    //console.log(jsonObj1[i].season);
    let matchid = matchesdata[index].id;
    //console.log(jsonObj1[i].id);
    for( let jindex= 0;jindex<deliveriesdata.length;jindex++)
    {//closing
    //console.log(jsonObj[j].match_id);
    if(matchid === deliveriesdata[jindex].match_id)
    {//closing
    let teamname = deliveriesdata[jindex].bowling_team;
    if(op[teamname] === undefined)
    {//closing
    op[teamname] = parseInt(deliveriesdata[jindex].extra_runs);
    }
    else
    {
    op[teamname] = op[teamname] + parseInt(deliveriesdata[jindex].extra_runs);  
    }
    }
    }
    }
  }
  //console.log(op);
fs.writeFile('../output/output2.json', JSON.stringify(op), 'utf-8', function(err) {
  if (err) throw err
console.log("completed");
})
}


function matchesOwnByTeam(matchesdata)
{
	let op = {} ;  //op means output
	for(let index=0;index<matchesdata.length;index++) 
	{
		let count=0;
		let teamname = matchesdata[index].winner;
		//console.log(a);
		let seasonname = matchesdata[index].season;
		//console.log(s);
		for(const atribute in op[seasonname])
		{
			//console.log(at);
			if(atribute === teamname) {
				count = count+1;
			}
		}
		if(op[seasonname] === undefined )
			op[seasonname] = {};
		if(count == 0)
		{
			op[seasonname][teamname] = 1;
		}
		else
		{
			op[seasonname][teamname] += 1;
		}
	}
	//console.table(op)
fs.writeFile('../output/output1.json', JSON.stringify(op), 'utf-8', function(err) {
	if (err) throw err
	console.log('writting completed')
})
}


function topTenEconomicalBowlers(matchesdata,deliveriesdata)
{
	let runs  ={} ;
	let overs ={} ;
 	for(let index=0;index<matchesdata.length;index++) 
	{
		if(matchesdata[index].season === '2015' )
		{
		//console.log(jsonObj1[i].season);
		let matchid = matchesdata[index].id;
		//console.log(jsonObj1[i].id);
		for( let jindex= 0;jindex<deliveriesdata.length;jindex++)
		{//closing
		//console.log(jsonObj[j].match_id);
		if(matchid === deliveriesdata[jindex].match_id)
		{//closing
		let bowlername = deliveriesdata[jindex].bowler;
		if(runs[bowlername] === undefined)
		{
			runs[bowlername] = parseInt(deliveriesdata[jindex].total_runs);
		}
		else
		{
			runs[bowlername] += parseInt(deliveriesdata[jindex].total_runs);
		}
		if(overs[bowlername] === undefined)
		{
			//overs[bowlername] = 0 ;
			if(deliveriesdata[jindex].wide_runs === '0' && deliveriesdata[jindex].noball_runs === '0'  )
			{
				overs[bowlername] = 1;
			}
			else {
				overs[bowlername] = 0;
			}
		}
		else
		{
			if(deliveriesdata[jindex].wide_runs === '0' && deliveriesdata[jindex].noball_runs === '0'  )
			{
				overs[bowlername] += 1;
			}
		}
		}
		}
		}
		}
	let economy = {};
	for(const key in overs)
	{
		economy[key]=(runs[key]/overs[key])*6;
	}
	const economyarray = Object.entries(economy);
	economyarray.sort(function(a,b){
    return a[1] - b[1];
	 });
console.log(economyarray.slice(0, 10));
fs.writeFile('../output/output.json', economyarray.slice(0,10), 'utf-8', function(err) {
	if (err) throw err
console.log("completed");
});
}




